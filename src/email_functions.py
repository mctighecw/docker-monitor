#!/usr/bin/python3
from smtplib import SMTP
from operator import itemgetter

from util_functions import get_env_vars, get_current_time

env_vars = get_env_vars()
ENV, SERVER_NAME, SENDER, RECIPIENT, GMAIL_USERNAME, GMAIL_PASSWORD = itemgetter("ENV", \
    "SERVER_NAME", "SENDER", "RECIPIENT", "GMAIL_USERNAME", "GMAIL_PASSWORD" )(env_vars)


def msg_body_template(message):
    date_time = get_current_time()

    footer = "- This message is autogenerated -"
    body = ("Status:\r\n%s\r\n\r\nTimestamp:\r\n%s\r\n\r\n%s"
       % (message, date_time, footer))

    return body


def send_email(type, message):
    subject = f"[{SERVER_NAME}] {type} Update"
    body = msg_body_template(message)

    msg = ("From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n%s"
       % (SENDER, RECIPIENT, subject, body))

    s = SMTP("smtp.gmail.com", 587)
    s.ehlo()

    if ENV == "test":
        s.set_debuglevel(1)

    if s.has_extn("STARTTLS"):
        s.starttls()
        s.ehlo()

    try:
        s.login(GMAIL_USERNAME, GMAIL_PASSWORD)
        s.sendmail(SENDER, [RECIPIENT], msg)
        print("Email sent successfully")
    except:
        print("Error sending email")
    finally:
        s.quit()
