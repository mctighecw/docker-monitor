#!/usr/bin/python3
import sys
from email_functions import send_email

# Note: need to run command
# python3 /docker-monitor/src/send_update_email.py "Docker" "Error message"

# Arguments
type = sys.argv[1]
message = sys.argv[2]

send_email(type, message)
