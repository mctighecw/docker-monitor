#!/usr/bin/python3
from datetime import datetime
import pytz

def get_env_vars():
    path = "/docker-monitor/src/.env"

    with open(path, "r") as f:
        env_dict = dict(tuple(line.replace("\n", "").split("=")) for line
            in f.readlines() if not line.startswith("#"))

    return env_dict


def get_current_time():
    local_tz = pytz.timezone("Europe/Berlin")
    time_now = datetime.now().replace(tzinfo=pytz.utc).astimezone(local_tz)
    res = time_now.strftime("%Y-%m-%d, %H:%M")

    return res
