#!/bin/bash

# Note: need to run command with bash
# $ bash /docker-monitor/docker_check.sh

# Check if Docker is running
if [ "$(systemctl is-active docker)" = "active" ]; then
  echo "Docker is running"
else
  DOCKER_ERROR="Docker is not running"
  echo $DOCKER_ERROR

  # Send email
  python3 /docker-monitor/src/send_update_email.py "Docker" "$DOCKER_ERROR"

  # Stop script
  exit 1
fi

# Names of all Docker containers to check
ALL_CONTAINERS=(
  "app-frontend"
  "app-backend"
  "mongodb"
  "proxy"
)

OFFLINE_CONTAINERS=()

# Check if each container exists and if it is running or not
for CONTAINER in "${ALL_CONTAINERS[@]}"; do
  if [ ! "$(docker ps -a | grep $CONTAINER)" ]; then
    echo "$CONTAINER not found"
    OFFLINE_CONTAINERS+=($CONTAINER)
  elif [ "$(docker container inspect -f '{{.State.Status}}' $CONTAINER)" != "running" ]; then
    echo "$CONTAINER not running"
    OFFLINE_CONTAINERS+=($CONTAINER)
  else
    echo "$CONTAINER is running"
  fi
done

if [ "${#OFFLINE_CONTAINERS[@]}" -gt 0 ]; then
  OFFLINE_STRING="${#OFFLINE_CONTAINERS[@]} container(s) offline:"

  for OFFLINE_CONTAINER in "${OFFLINE_CONTAINERS[@]}"; do
    OFFLINE_STRING+=" $OFFLINE_CONTAINER"
  done

  # Send email
  python3 /docker-monitor/src/send_update_email.py "Docker" "$OFFLINE_STRING"
fi

exit 0
