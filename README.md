# README

This is a simple template to monitor Docker containers running on Linux (_Ubuntu_/_Debian_) servers, with email notifications.

Email can be configured using a [Gmail](https://www.google.com/gmail) account.

The Shell script `docker_check.sh` will check if:
- Docker is running
- all of the specified containers are present and running

It can be run manually or added to the `crontab` to run at specified intervals.

Currently it is configured to only send an email if there is an error (i.e. Docker is not running or at least one container is stopped). If everything is working, no email will be sent out.

## App Information

App Name: docker-monitor

Created: April 2021

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/docker-monitor)

License: MIT License

## Tech Stack

- Shell
- Python 3.x
- Gmail

## Set Up

1. Add Python timezone module

```
$ pip3 install pytz
```

2. Enter Gmail details in the `src/.env` file.

_Note_: You need to log into Gmail and enable "Less secure app access"

3. Put the names of the Docker containers to check into the `ALL_CONTAINERS` variable in `docker_check.sh`.

4. Add correct timezone in `src/util_functions.py`

5. Set up a `crontab` (optional)

```
$ crontab -e

# Check Docker status every 6 hrs
0 */6 * * * bash /docker-monitor/docker_check.sh
```

6. Make sure the paths in the various files are correct (replace `/docker-monitor/...`)

## Caveat

I'm not an expert in this area, so I do not take any responsibility if something goes wrong. Please check over the code and use it at your own discretion. I welcome feedback about possible improvement.

Last updated: 2025-01-15
